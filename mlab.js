var existingTags = new Array('me', 'myself', 'and', 'i');
var isAdvancedUpload = function() {
    var div = document.createElement('div');
    return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div) && 'FormData' in window && 'FileReader' in window;
}();

$(function() {
    var $form = $('.box');
    var $input = $form.find('input[type=file]'),
        $label = $form.find('label'),
        showFiles = function(files) {
            $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace('{count}', files.length) : files[0].name);
        };

    if (isAdvancedUpload) {
        var droppedFiles = false;

        $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
        }).on('dragover dragenter', function() {
            $form.addClass('is-dragover');
        }).on('dragleave dragend drop', function() {
            $form.removeClass('is-dragover');
        }).on('drop', function(e) {
            droppedFiles = e.originalEvent.dataTransfer.files;
            showFiles(droppedFiles);
            $form.trigger('submit');
        });
        $form.addClass('has-advanced-upload');
    }

    $input.on('chane', function(e) {
        showFiles(e.target.files);
        $form.trigger('submit');
    });

    $form.on('submit', function(e) {
        if ($form.hasClass('is-uploading')) return false;

        $form.addClass('is-uploading').removeClass('is-error');

        if (isAdvancedUpload) {
            e.preventDefault();

            var ajaxData = new FormData($form.get(0));

            if (droppedFiles) {
                $.each(droppedFiles, function(i, file) {
                    ajaxData.append($input.attr('name'), file);
                });
            }

            $.ajax({
                url: $form.attr('action'),
                type: $form.attr('method'),
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                complete: function() {
                    $form.removeClass('is-uploading');
                },
                success: function(data) {
                    $form.addClass(data.success == true ? 'is-success' : 'is-error');
                    if (!data.success) $errorMsg.text(data.error);
                    nextStep();
                },
                error: function(e) {
                    console.log(e);
                }
            });
        } else {
            var iframeName = 'uploadiframe'+new Date().getTime;
            $iframe = $('<iframe name="'+iframeName+'" style="display:none;"></iframe>');

            $('body').append($iframe);
            $form.attr('target', iframeName);

            $iframe.one('load', function() {
                var data = JSON.parse($iframe.contents().find('body').text());
                $form.removeClass('is-uploading')
                    .addClass(data.success == true ? 'is-success' : 'is-error')
                    .removeAttr('target');
                if (!data.success) $errorMsg.text(data.error);
                $form.removeAttr('target');
                $iframe.remove();
                nextStep();
            });
        }
    });
    $('.supplier-form-left input[type=text], textarea').keypress(function() {
        if ($(this).val().length > 0) {
            $(this).prev().hide();
        } else {
            $(this).prev().show();
        }
    });
    
    $('#tag-save').click(function() {
        var phrase = $('#tag-phrase').val();
        if (phrase.length > 0) {
            addTag(phrase);
        }
    });
    $('#tag-phrase').keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            addTag($(this).val());
            return false;  
        }
    });
});

function nextStep() {
    $('.step-1').slideUp();
    $('.step-2').slideDown();
}

function addTag(phrase) {
    var container = $('#tags');
    container.prepend('<li><a href="javascript:void(0);" class="button">'+phrase.toLowerCase()+' <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>');
    $('#tag-phrase').val('');
}