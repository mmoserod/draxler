<!DOCTYPE html>
<html>
    <head>
        <title>PDF Upload</title>
        <script type="text/javascript" src="modernizr-custom.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <form class="box" method="post" action="parser.php" enctype="multipart/form-data">
            <div class="box__input">
                <input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" multiple />
                <label for="file"><strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
                <button class="box__button" type="submit">Upload</button>
            </div>
            <div class="box__uploading">Uploading&hellip;</div>
            <div class="box__success">Done!</div>
            <div class="box__error">Error! <span></span>.</div>
        </form>
        <style>
            .box__dragndrop,
            .box__uploading,
            .box__success,
            .box__error {
                display: none;
            }
            .box.has-advanced-upload {
                background-color: white;
                outline: 2px dashed black;
                outline-offset: -10px;
            }
            .box.has-advanced-upload .box__dragndrop {
                display: inline;
            }
            .box.is-dragover {
                background-color: grey;
            }
            .box {
                width: 500px;
                height: 300px;
                text-align: center;
            }
            .box__file, .box__button {
                display: none;
            }
            label[for=file] {
                margin-top: 140px;
                display: inline-block;
            }
            .box.is-uploading .box__input {
                visibility: none;
            }
            .box.is-uploading .box__uploading {
                display: block;
            }
            .no-js .box__button {
                display: block;
            }
        </style>
        <script>
            var isAdvancedUpload = function() {
                var div = document.createElement('div');
                return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div) && 'FormData' in window && 'FileReader' in window;
            }();
            
            $(function() {
                var $form = $('.box');
                var $input = $form.find('input[type=file]'),
                    $label = $form.find('label'),
                    showFiles = function(files) {
                        $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace('{count}', files.length) : files[0].name);
                    };
            
                if (isAdvancedUpload) {
                    var droppedFiles = false;

                    $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                    }).on('dragover dragenter', function() {
                        $form.addClass('is-dragover');
                    }).on('dragleave dragend drop', function() {
                        $form.removeClass('is-dragover');
                    }).on('drop', function(e) {
                        droppedFiles = e.originalEvent.dataTransfer.files;
                        showFiles(droppedFiles);
                        $form.trigger('submit');
                    });
                    $form.addClass('has-advanced-upload');
                }
                
                $input.on('chane', function(e) {
                    showFiles(e.target.files);
                    $form.trigger('submit');
                });
                
                $form.on('submit', function(e) {
                    if ($form.hasClass('is-uploading')) return false;
                    
                    $form.addClass('is-uploading').removeClass('is-error');
                    
                    if (isAdvancedUpload) {
                        e.preventDefault();
                        
                        var ajaxData = new FormData($form.get(0));
                        
                        if (droppedFiles) {
                            $.each(droppedFiles, function(i, file) {
                                ajaxData.append($input.attr('name'), file);
                            });
                        }
                        
                        $.ajax({
                            url: $form.attr('action'),
                            type: $form.attr('method'),
                            data: ajaxData,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            complete: function() {
                                $form.removeClass('is-uploading');
                            },
                            success: function(data) {
                                $form.addClass(data.success == true ? 'is-success' : 'is-error');
                                if (!data.success) $errorMsg.text(data.error);
                            },
                            error: function(e) {
                                console.log(e);
                            }
                        });
                    } else {
                        var iframeName = 'uploadiframe'+new Date().getTime;
                        $iframe = $('<iframe name="'+iframeName+'" style="display:none;"></iframe>');
                        
                        $('body').append($iframe);
                        $form.attr('target', iframeName);
                        
                        $iframe.one('load', function() {
                            var data = JSON.parse($iframe.contents().find('body').text());
                            $form.removeClass('is-uploading')
                                .addClass(data.success == true ? 'is-success' : 'is-error')
                                .removeAttr('target');
                            if (!data.success) $errorMsg.text(data.error);
                            $form.removeAttr('target');
                            $iframe.remove();
                        });
                    }
                });
            });
        </script>
    </body>
</html>