<!DOCTYPE html>
<html>
    <head>
        <title>MLab</title>
        <link rel="stylesheet" href="wp-admin-theme.css" />
        <link rel="stylesheet" href="default.css" />
        <script type="text/javascript" src="modernizr-custom.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body class="wp-admin wp-core-ui js  index-php auto-fold admin-bar branch-4-7 version-4-7-2 admin-color-fresh locale-en-us customize-support sticky-menu svg">
        <main>
            <div class="postbox hndle ui-sortable-handle">
                <div class="inside">
                    <h2>ADD</h2>
                    
                        <div class="supplier-info">
                            <div class="supplier-form-left">
                                <form method="post" action="parser.php" class="initial-form hide-if-no-js step-3" enctype="multipart/form-data">
                                    <div class="input-text-wrap">
                                        <label for="supplier_name" class="prompt">Name</label>
                                        <input type="text" name="supplier_name" id="supplier_name" maxlength="255" required autocomplete="off" />
                                    </div>
                                    <fieldset>
                                        <legend>Details</legend>
                                        <ul>
                                            <li><label>Phone:</label><input type="text" /></li>
                                            <li><label>Address:</label><input type="text" /></li>
                                            <li><label>Website:</label><input type="text" /></li>
                                            <li><label>Contact Person:</label><input type="text" /></li>
                                        </ul>
                                        <a href="javascript:void(0);" class="button">+ new field</a>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="supplier-form-right">
                                <div class="supplier-brochure">
                                    <label for="brochure">+ADD BROCHURE</label>
                                    <input type="file" name="brochure" id="brochure" />
                                    <fieldset>
                                        <legend>tags</legend>
                                        <input type="text" name="brand" id="tag-phrase" placeholder="type a brand name here" />
                                        <a href="javascript:void(0);" class="button button-primary" id="tag-save">+</a>
                                        <ul id="tags">
                                            <li><a href="javascript:void(0);" class="button">lakeshore carpets <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>
                                            <li><a href="javascript:void(0);" class="button">idl <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>
                                            <li><a href="javascript:void(0);" class="button">la bonita <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>
                                            <li><a href="javascript:void(0);" class="button">trekk+ <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>
                                            <li><a href="javascript:void(0);" class="button">vdchairs <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>
                                            <li><a href="javascript:void(0);" class="button">vanilla tile solutions <svg width="12" height="12" viewBox="0 0 16 16"><path d="M2.343 13.657c-3.124-3.124-3.124-8.19 0-11.314 3.125-3.124 8.19-3.124 11.315 0s3.124 8.19 0 11.314c-3.125 3.125-8.19 3.125-11.315 0zM12.243 3.757c-2.344-2.343-6.143-2.343-8.485 0s-2.344 6.142 0 8.485c2.343 2.343 6.142 2.343 8.485 0s2.343-6.142 0-8.485zM5.879 11.536l-1.414-1.415 2.121-2.121-2.121-2.121 1.414-1.415 2.121 2.122 2.121-2.122 1.414 1.415-2.121 2.121 2.121 2.121-1.414 1.415-2.121-2.122-2.121 2.122z"></path></svg></a></li>
                                        </ul>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="submit">
                            <input type="submit" class="button button-primary button-large" value="Save" />
                        </div>
                    <form class="box step-1" method="post" action="parser.php" enctype="multipart/form-data">
                        <div class="box__input">
                            <input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" multiple />
                            <label for="file" class="wrap"><strong class="page-title-action">Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
                            <button class="box__button" type="submit">Upload</button>
                        </div>
                        <div class="box__uploading">Uploading&hellip;</div>
                        <div class="box__success">Done!</div>
                        <div class="box__error">Error! <span></span>.</div>
                    </form>
                    <form class="step-2">
                        <ul>
                            <li><img src="pdf.png" height="150" /></li>
                            <li><img src="chair.jpg" height="150" /></li>
                            <li><img src="chair2.jpg" height="150" /></li>
                            <li>
                                <label for="supplier_thumbnails">+ Add More</label>
                                <input type="file" name="supplier_thumbnails" id="supplier_thumbnails" />
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <ul class="stack">
                <li class="card">
                    <aside>
                        <img src="chair.jpg" width="130" />
                        <input type="text" class="image-title" value="chair.jpg" />
                    </aside>
                    <fieldset>
                        <legend>tags</legend>
                        <input type="text" name="supplier_tag" id="supplier_tag" maxlength="255" placeholder="type a new tag here" />
                        <a href="javascript:void(0);" class="button button-primary">+</a>
                        <ul id="supplier_tags" class="wrap">
                            <li class="page-title-action">chair</li>
                            <li class="page-title-action">white</li>
                            <li class="page-title-action">wood</li>
                            <li class="page-title-action">classic</li>
                        </ul>
                    </fieldset>
                </li>
                <li class="card">
                    <a href="javascript:void(0);">+ ADD MORE</a>
                </li>
            </ul>
        </main>
        <script type="text/javascript" src="mlab.js"></script>
    </body>
</html>