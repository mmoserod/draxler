<?php
die(json_encode(['success'=>true]));
define('DBHOST', '127.0.0.1');
define('DBNAME', 'mlab');
define('DBUSER', 'root');
define('DBPASS', '');
define('DBCSET', 'utf8');

date_default_timezone_set('Asia/Singapore');

require_once "vendor/autoload.php";

class MLab
{
    public $db;
    public $tblSupplier = 'lab_supplier';
    public $tblTags = 'lab_tags';
    public $tblSupplierTags = 'lab_supplier_tags';

    public function __construct()
    {
        $this->db = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
        
        if ($this->db->connect_errno > 0) {
            printf("Connection failed: %s\n", mysqli_connect_error());
            exit();
        }
    }
    
    public function getSupplier($id = 0)
    {
        $supplier = array();
        
        if ($id > 0) {
            if ($stmt = $this->db->prepare("SELECT * FROM `{$tblSupplier}` s WHERE s.`id` = ?")) {
                $stmt->bind("i", $id);
                $result = $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $supplier[] = $row;
                    }
                }
            }
        }

        return $supplier;
    }
    
    public function set()
    {
        
    }
    
    public function sanitize()
    {
        if (!array_key_exists('files', $_POST) || empty($_POST['files'])) {
            return false;
        }
        if (!array_key_exists('supplier_name', $_POST) || empty($_POST['supplier_name'])) {
            return false;
        }
    }
}

$parser = new \Smalot\PdfParser\Parser();
try {
    $pdf = $parser->parseFile('1.6.pdf');
    $text = $pdf->getDetails();
    echo json_encode($text);
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
